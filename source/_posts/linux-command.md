---
title: Linux Command
date: 2019-10-08 16:17:41
tags: [linux, base]
category: [linux]
---

自由、免费、可靠、安全、稳定，以网络为核心的设计思想，继承以`Unix`性能稳定多用户、多任务、多线程、多CPU的操作系统

* 查看当前操作系统发行版详细信息

```base
[root@tianlin /]# lsb_release -a
Distributor ID: CentOS
Description:    CentOS Linux release 7.6.1810 (Core) 
Release:    7.6.1810
Codename:   Core 
```

* 查看当前操作系统内存硬件信息

```base
[root@tianlin /]# dmidecode -t memory
# dmidecode 3.1
Getting SMBIOS data from sysfs.
SMBIOS 2.8 present.

Handle 0x1000, DMI type 16, 23 bytes
Physical Memory Array
    Location: Other
    Use: System Memory
    Error Correction Type: Multi-bit ECC
    Maximum Capacity: 8 GB
    Error Information Handle: Not Provided
    Number Of Devices: 1

Handle 0x1100, DMI type 17, 40 bytes
Memory Device
    Array Handle: 0x1000
    Error Information Handle: Not Provided
    Total Width: Unknown
    Data Width: Unknown
    Size: 8192 MB
    Form Factor: DIMM
    Set: None
    Locator: DIMM 0
    Bank Locator: Not Specified
    Type: RAM
    Type Detail: Other
    Speed: Unknown
    Manufacturer: Alibaba Cloud
    Serial Number: Not Specified
    Asset Tag: Not Specified
    Part Number: Not Specified
    Rank: Unknown
    Configured Clock Speed: Unknown
    Minimum Voltage: Unknown
    Maximum Voltage: Unknown
    Configured Voltage: Unknown 
```

* 查看当前操作系统硬盘和分区分布

```base
[root@tianlin /]# lsblk
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
vda    253:0    0  300G  0 disk 
└─vda1 253:1    0  300G  0 part /
Codename:   Core 
```

* `Cron`是一个可以在`Unix/Linux`等操作系统执行强大的任务计划程序，通过设置设定的时间运行命令实现脚本自动化

```base
minute  hour    day   month   dayofweek command
<分钟>  <小时>  <日>  <月份>  <星期>  <命令>

#每天凌晨0点执行
00 00 * * * /server/php7.3.9/php /htdocs/example.web.com/action/post/push.php
#每10分钟执行一次
*/10 * * * * /server/php7.3.9/php /htdocs/example.web.com/action/post/total.php
#每小时执行
0 * * * * /server/php7.3.9/php /htdocs/example.web.com/action/post/example.php
#每天执行
0 0 * * * /server/php7.3.9/php /htdocs/example.web.com/action/post/example.php
#每周执行
0 0 * * 0 /server/php7.3.9/php /htdocs/example.web.com/action/post/example.php
#每月执行
0 0 1 * * /server/php7.3.9/php /htdocs/example.web.com/action/post/example.php
#每年执行
0 0 1 1 * /server/php7.3.9/php /htdocs/example.web.com/action/post/example.php
```
