---
title: Git Command
date: 2019-10-09 14:59:04
tags: [git, base]
category: [git]
---

分布式版本控制器`Git`，轻量级、多分支、易管理、性能高等。如果说`SVN`是自动步枪，那么 `Git`就是加特林；如果说`SVN`是一阳指，那么`Git`就是六脉神剑。总之就是很牛叉牛的飞起...

* 配置本地`Git`库

```base
#<你的用户名>
$ git config --global user.name "Your Name"
#<你所用邮箱>
$ git config --global user.email "Your@Email.com"
#<依次执行配置用户名和用户邮箱之后，本地Git仓库都会使用这个配置>
```

* 创建非对称`RSA`加密`SSH Key`

```base
$ ssh-keygen -t rsa -C "Your@Email.com"
#<执行命令后会在电脑用户主目录下生成.ssh文件，有id_rsa和id_rsa.pub两个文件不管你用的是GitHub还是用了自己搭配的Git服务器，打
#开id_rsa.pub公钥全选复制，粘贴到Git服务器上面添加Add SSH Key的地方保存，这样本地Git和服务器Git关联起来了。>
#<检测连接是否成功>
$ ssh -T git@github.com
```

* 从远程主机克隆一个版本库到本地`git clone`

```base
$ git clone <版本库的网址>
$ git clone <版本库的网址> <本地目录名>
#<例如>
$ git clone git@github.com:example/example.git #<这样会在本地主机生成一个目录，与远程主机的版本库同名>
$ git clone git@github.com:example/example.git localExample #<如果这样执行，结果本地版本库和远程的就是不同目录名了>
#<git clone支持多种协议，除了SSH)以外，还支持HTTPS、Git、本地文件协议等>
```
